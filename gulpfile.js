var gulp = require('gulp');
var uglify = require('gulp-uglify');
var webpack = require('webpack-stream');
var zip = require('gulp-zip');
var forceDeploy = require('gulp-jsforce-deploy');
var build = require('gulp-build');
var ts = require("gulp-typescript");
var concat = require ("gulp-concat");
var rename = require ("gulp-rename");


// currently having issues with build and uglify using latest webpack versio
//#Unhandled rejection TypeError: chunk.hasRuntime is not a function
//transpiling to javascript
gulp.task('build',function(){
    return gulp.src('src/**/*.ts')
        .pipe(ts({
            noImplicitAny: true,
            outFile: 'output.js'
        }))
        .pipe (gulp.dest('dist'))
//    return webpack(require('./webpack.config.js'))
  //      .pipe(gulp.dest('npm run build'));

});


//get rid of all comments and space
gulp.task('uglify',function(){
    return gulp.src('dist/**/*.js')
        .pipe(concat('main.js'))
        .pipe(rename({suffix:'.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});



//to do  zip vendor 
//need to create a separate gulp task to create seprate static resource
// static resources need to be less than 5MB for deployment to salesforce
gulp.task('zip-vendor',function(){

    return gulp.src('dist/*vendor.**')
        .pipe(zip('vendor.resource'))
        .pipe(gulp.dest('pkg/staticresources/'))
})


//to do zip inline
//need to create a separate gulp task to create seprate static resource
gulp.task('zip-inline',function(){

    return gulp.src('dist/*inline.**')
        .pipe(zip('inline.resource'))
        .pipe(gulp.dest('pkg/staticresources/'))
})


//to do zip styles
gulp.task('zip-styles',function(){

    return gulp.src('dist/*styles.**')
        .pipe(zip('styles.resource'))
        .pipe(gulp.dest('pkg/staticresources/'))
})

//to do zip polyfills
gulp.task('zip-polyfills',function(){

    return gulp.src('dist/*polyfills.**')
        .pipe(zip('polyfills.resource'))
        .pipe(gulp.dest('pkg/staticresources/'))
})

//to do zip main
gulp.task('zip-main',function(){

    return gulp.src('dist/*main.**')
        .pipe(zip('main.resource'))
        .pipe(gulp.dest('pkg/staticresources/'))
})

//zip it
//currently being used to zip up everything in the dist folder
//should be converted to a task that combines all zip tasks above
//eg gulp.task('zipbundles',['zip1','zip2', 'zip3' .....]);
gulp.task('zip', ['zip-main', 'zip-polyfills', 'zip-styles', 'zip-inline', 'zip-vendor']);

    /*return gulp.src('dist/*.**')
        .pipe(zip('angular2.resource'))
        .pipe(gulp.dest('pkg/staticresources/'))*/


gulp.task('zip-test',function(){

    return gulp.src('dist/*.**')
        .pipe(zip('angular2.resource'))
        .pipe(gulp.dest('pkg/staticresources/'))
})

/*forceDeploy = (username, password){
        conn = new jsforce.connection()
        conn.login username, password
        conn.metadata.deploy(file.contents).complete(details:true)
        .then (res) ->
            if res.details?.componentFailures
                console.error res.details?.componentFailures
                return callback(new Error('Deploy Failed.'))
            callback()
            , (err) ->
                console.error(err)
                callback(err)
}*/
//deploy it
gulp.task('deploy',function(){
    return gulp.src('./pkg/**', { base: "." })
        .pipe(zip('pkg.zip'))
        .pipe(gulp.dest('pkg/staticresources/'))
        .pipe(forceDeploy({
            username:'salesforceangular@gmail.com',
            password:'angular@@#@@15XJmfrRnvJjWcdpihndi9Jy2F',
            loginUrl:'https://login.salesforce.com',
            version:'36.0'
        }))
})




