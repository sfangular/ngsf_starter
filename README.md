# ngsf_starter

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.1.
This project can be used as a starter kit to begin Angular2/4 Development with Salesforce

## Pre-requistes 
node 6.9.x or higher and npm 3.x.x or higher must be installed on the development machine
- Use the command node -v and npm -v to check version numbers 
- MAC OSX node installation/update command 
- Windows node installation/update command 
- MAC OX npm installation/update 
- Windows npm installation/update


## Development server

Run npm run build and npm run start. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `npm build run` to build the project. The build artifacts will be stored in the `dist/` directory.

## Deploy to Salesforce
Run the commands gulp zip and gulp deploy to deploy to Salesforce. 
To change Salesforce credentials and URL modify the gulpfile.js


## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
