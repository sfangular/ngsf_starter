import {
    NgModel
} from "./ng-model";
import {
    SalesforceModel
} from "./salesforce-model";

export class NgSfMap {

    public mapToSalesforceObjectModel(angularData: NgModel): SalesforceModel{
        let salesforceData: SalesforceModel = new SalesforceModel();
        salesforceData.address = angularData.address;
        salesforceData.birthdate = angularData.birthdate;
        salesforceData.email = angularData.email;
        salesforceData.name = angularData.name;
        return salesforceData;
    }
}

