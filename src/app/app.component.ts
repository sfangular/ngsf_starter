import { Component } from '@angular/core';
import {NgModel} from './ng-model';
import {SaveService} from './save.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [SaveService]
})
export class AppComponent {
  contact: NgModel = new NgModel();

  constructor(protected _saveService: SaveService){

  }
  title = 'to the angular salesforce nation!!!';
  User = this.contact.name? this.contact.name : null;

  private saveContact (){
    console.log("save contact");
   this._saveService.saveInformation(this.contact); 

  }
}
