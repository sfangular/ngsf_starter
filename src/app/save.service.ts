import { Injectable } from '@angular/core';
import {Observer, Observable} from 'rxjs';
import {NgModel} from './ng-model';

declare var VisualForce: any; 
@Injectable()
export class SaveService {
  public saveObserver: Observer<any>;
  public saveChange: Observable<any>;

  constructor() { 
    this.saveChange = new Observable(observer => this.saveObserver = observer).share();
  }


  public saveInformation(model: NgModel){
    console.log("saving info");
    this.saveService(model,
    this.saveObserver)
  }

  private saveService(model: NgModel, JSRemoteChangeObserver: Observer<any>){

    try {
      VisualForce.remoting.Manager.invokeAction(
        "AngularPageController.saveInformationCall",
        model,
        function (response, event){
          if (event.status) {
            JSRemoteChangeObserver.next(response);
          }
          else {
            console.log("error");
          }
        },
        {buffer: true, escape: false, timeout: 10000});
    
  }
  finally {
    console.log("not remoting");
  }

}

}

