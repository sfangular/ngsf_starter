import { NgsfAppPage } from './app.po';

describe('ngsf-app App', () => {
  let page: NgsfAppPage;

  beforeEach(() => {
    page = new NgsfAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
